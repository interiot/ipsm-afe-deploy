# IPSM-AFE Deployment

In order to run IPSM-AFE, run the `ipsm-afe-assembly-1.52.jar` file.

```bash
java -jar ipsm-afe-assembly-1.52.jar
```

Next sections describe how to run application with user configuration.

```bash
java -Dconfig.file=ipsm-afe.conf -Dlogger.file=logback.xml -Dhttp.port=9090 -jar ipsm-afe-assembly-1.52.jar
```

## Configuration file
In order to change default configuration, edit the `ipsm-afe.config` and run the `ipsm-afe-assembly-1.52.jar` file with the parameter: `-Dconfig.file`.
```bash
java -Dconfig.file=ipsm-afe.conf -jar ipsm-afe-assembly-1.52.jar
```

## Port
Default port of IPSM is `9000`. To setup port, run the `ipsm-afe-assembly-1.52.jar` file with the parameter: `-Dhttp.port`.
```bash
java -Dhttp.port=9090 -jar ipsm-afe-assembly-1.52.jar
```

## Logger configuration
The default level of logger is `debug`. In order to change the default logger configuration, find and setup `logback.xml` file. To run IPSM-AFE with configured logger, run the `ipsm-afe-assembly-1.52.jar` file with the parameter: `-Dlogger.file`.
```bash
java -Dlogger.file=logback.xml -jar ipsm-afe-assembly-1.52.jar
```

## IPSM
IPSM-AFE uses IPSM in order to validate an alignment. IPSM URL and port configuration is in `ipsm-afe.config` file. To change default configuration, find and set up two parameters:
```bash
ipsm.url=127.0.0.1
ipsm.port=8888
```
